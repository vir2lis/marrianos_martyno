from django.contrib import admin
from .models import City, CityTimeZone, User, TimeZone, Form, Label, Language
from .models import PaymentStatus, Payment, Flow, Level, StudentStatus, Coach, Student, Team, TeamMember, ListCategory,\
    BonusCategory, TaskCategory, FulfillmentStatus


admin.site.register(City)
admin.site.register(CityTimeZone)
admin.site.register(User)
admin.site.register(TimeZone)
admin.site.register(Form)
admin.site.register(Label)
admin.site.register(Language)
admin.site.register(PaymentStatus)
admin.site.register(Payment)
admin.site.register(Flow)
admin.site.register(Level)
admin.site.register(StudentStatus)
admin.site.register(Coach)
admin.site.register(Student)
admin.site.register(Team)
admin.site.register(TeamMember)
admin.site.register(ListCategory)
admin.site.register(BonusCategory)
admin.site.register(TaskCategory)
admin.site.register(FulfillmentStatus)
