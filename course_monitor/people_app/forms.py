from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import models


from course_monitor.people_app.models import User, City, CityTimeZone, TimeZone, ListCategory, Flow, TaskCategory, \
    Payment, BonusCategory, PaymentStatus, Level, StudentStatus, Student

help_text = 'Your password can’t be too similar to your other personal information.' + \
            'Your password must contain at least 8 characters.\n' \
           'Your password can’t be a commonly used password.\n Your password can’t be entirely numeric.'

OFFSET_CHOICES = [
    ('+12', '+12'),
    ('+11', '+11'),
    ('+10', '+10'),
    ('+9', '+9'),
    ('+8', '+8'),
    ('+7', '+7'),
    ('+6', '+6'),
    ('+5', '+5'),
    ('+4', '+4'),
    ('+3', '+3'),
    ('+2', '+2'),
    ('+1', '+1'),
    ('0', '0'),
    ('-1', '-1'),
    ('-2', '-2'),
    ('-3', '-3'),
    ('-4', '-4'),
    ('-5', '-5'),
    ('-6', '-6'),
    ('-7', '-7'),
    ('-8', '-8'),
    ('-9', '-9'),
    ('-10', '-10'),
    ('-11', '-11'),
    ('-12', '-12')
]


class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label="Username")
    password1 = forms.CharField(widget=forms.PasswordInput, label="New Password")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Repeat password")
    account_name = forms.CharField(label="Account name")
    email = forms.CharField(widget=forms.EmailInput, label="Email")

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'account_name', 'email']


class UpdateUserForm(forms.ModelForm):
    new_social_network_link = forms.CharField(max_length=30, required=False, label="New network")
    link = forms.CharField(max_length=255, required=False, label="New link")

    class Meta:
        model = User
        fields = ['username', 'last_login', 'groups', 'first_name', 'last_name', 'account_name', 'nick', 'name', 'phone',
                  'sex', 'email', 'city', 'comment', 'link_to_social_network']

    def clean(self):
        new_social_network_link = self.cleaned_data.get('new_social_network_link')
        link_to_social_network = self.cleaned_data.get('link_to_social_network')
        link = self.cleaned_data.get('link')
        if not new_social_network_link and not link_to_social_network:
            raise forms.ValidationError('Must specify either New social network link')
        elif not link_to_social_network:
            link_to_social_network, created = UserSocialNetwork.objects.get_or_create(name=new_social_network_link,
                                                                                      link=link)
            self.cleaned_data['link_to_social_network'] = link_to_social_network
        return super(UpdateUserForm, self).clean()


class CreateStudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['coach', 'flow', 'student_status', 'level']


class CreateCityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"


class UpdateCityTimeZoneForm(forms.ModelForm):
    class Meta:
        model = CityTimeZone
        fields = "__all__"
    offset = forms.ChoiceField(choices=OFFSET_CHOICES)
    new_city = forms.CharField(max_length=30, required=False, label="New City")
    description = forms.CharField(max_length=255, required=False, label="New Description")

    def __init__(self, *args, **kwargs):
        super(UpdateCityTimeZoneForm, self).__init__(*args, **kwargs)
        self.fields['city'].required = False

    def clean(self):
        new_city = self.cleaned_data.get('new_city')
        city = self.cleaned_data.get('city')
        description = self.cleaned_data.get('description')
        if not new_city and not city:

            # neither was specified so raise error to user
            raise forms.ValidationError('Must specify either city or city')
        elif not city:

            # get/create 'city' from 'city' and use it for 'city' field
            city, created = City.objects.get_or_create(name=new_city, description=description)
            self.cleaned_data['city'] = city

        return super(UpdateCityTimeZoneForm, self).clean()


class CreateTimeZoneForm(forms.ModelForm):
    class Meta:
        model = TimeZone
        fields = "__all__"


class CreateCityTimeZoneForm(forms.ModelForm):
    offset = forms.ChoiceField(choices=OFFSET_CHOICES)
    new_city = forms.CharField(max_length=30, required=False, label="New City")
    description = forms.CharField(max_length=255, required=False, label="New Description")

    class Meta:
        model = CityTimeZone
        fields = "__all__"

    # this makes city_id not required, we will check for city_id and city in clean method
    def __init__(self, *args, **kwargs):
        super(CreateCityTimeZoneForm, self).__init__(*args, **kwargs)
        self.fields['city'].required = False

    def clean(self):
        new_city = self.cleaned_data.get('new_city')
        city = self.cleaned_data.get('city')
        description = self.cleaned_data.get('description')
        if not new_city and not city:

            # neither was specified so raise error to user
            raise forms.ValidationError('Must specify either city or city_id')
        elif not city:

            # get/create 'city_id' from 'city' and use it for 'city_id' field
            city, created = City.objects.get_or_create(name=new_city, description=description)
            self.cleaned_data['city'] = city

        return super(CreateCityTimeZoneForm, self).clean()


class UpdateListCatForm(forms.ModelForm):

    # def __init__(self, *args, **kwargs):
    #     super(UpdateListCatForm, self).__init__(*args, **kwargs)
    #     self.fields['name'].widget.attrs['disabled'] = True

    class Meta:
        model = ListCategory
        fields = ['description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': 10, 'cols': 60}),
        }


class CreateFlowForm(forms.ModelForm):
    class Meta:
        model = Flow
        fields = "__all__"


class UpdateFlowForm(forms.ModelForm):
    class Meta:
        model = Flow
        fields = "__all__"


class UpdateTaskCategoryForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(UpdateTaskCategoryForm, self).__init__(*args, **kwargs)
    #     self.fields['name'].widget.attrs['disabled'] = True

    class Meta:
        model = TaskCategory
        fields = ['description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': 10, 'cols': 60}),
        }


class UpdateBonusCategoryForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(UpdateBonusCategoryForm, self).__init__(*args, **kwargs)
    #     self.fields['name'].widget.attrs['disabled'] = True

    class Meta:
        model = BonusCategory
        fields = ['description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': 10, 'cols': 60}),
        }


class UpdatePaymentStatusForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(UpdatePaymentStatusForm, self).__init__(*args, **kwargs)
    #     # self.fields['name'].widget.attrs['disabled'] = True

    class Meta:
        model = PaymentStatus
        fields = ['description']
        widgets = {
            'description': forms.Textarea(attrs={'rows': 10, 'cols': 60}),
        }


class CreateLevelForm(forms.ModelForm):
    class Meta:
        model = Level
        fields = '__all__'


class UpdateLevelForm(forms.ModelForm):
    class Meta:
        model = Level
        fields = '__all__'


class CreateStudentStatusForm(forms.ModelForm):
    class Meta:
        model = StudentStatus
        fields = '__all__'


class UpdateStudentStatusForm(forms.ModelForm):
    class Meta:
        model = StudentStatus
        fields = '__all__'


class CreatePaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = "__all__"

#
# class CreateSocialNetworkLinkForm(forms.ModelForm):
#     class Meta:
#         model = UserSocialNetwork
#         fields = '__all__'
