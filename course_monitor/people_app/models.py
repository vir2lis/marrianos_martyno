from django.db import models
from django.contrib.auth.models import AbstractUser, Group


class City(models.Model):
    name = models.CharField(max_length=30, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class TimeZone(models.Model):
    name = models.CharField(max_length=3, null=False, blank=False, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class CityTimeZone(models.Model):
    """
    must have city_id, time_zone_id
    """
    city = models.ForeignKey(City, on_delete=models.RESTRICT, null=False, blank=False)
    time_zone = models.ForeignKey(TimeZone, on_delete=models.RESTRICT, null=False, blank=False)
    offset = models.CharField(max_length=3, null=False, blank=False)

    def __str__(self):
        return f'City name ({self.city.name}) Name ({self.time_zone.name}) Offset ({self.offset})'


class UserSocialNetwork(models.Model):
    name = models.CharField(max_length=50)
    link = models.TextField()

    def __str__(self):
        return self.link


class User(AbstractUser):

    account_name = models.CharField(max_length=70, null=False, blank=False)
    nick = models.CharField(max_length=50, null=True, blank=True)
    name = models.CharField(max_length=70, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    sex = models.CharField(max_length=2, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.RESTRICT, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(null=True, blank=True)
    link_to_social_network = models.ManyToManyField(UserSocialNetwork)

    REQUIRED_FIELDS = ['username']
    USERNAME_FIELD = 'email'
# ==================================================


class Form(models.Model):
    """
    language choices
    """
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Language(models.Model):
    """
    language choices
    """
    name = models.CharField(max_length=10, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Label(models.Model):
    """
    language choices
    """
    form = models.ForeignKey(Form, on_delete=models.RESTRICT)
    language = models.ForeignKey(Language, on_delete=models.RESTRICT)
    label_code = models.CharField(max_length=50, null=True, blank=True)
    label_translated = models.CharField(max_length=255, null=True, blank=True)
# ========================================================================


class PaymentStatus(models.Model):
    """
    name = full, partial, free, waiting
    """
    name = models.CharField(max_length=20, null=False, blank=False, unique=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Payment(models.Model):
    payment_status = models.ForeignKey(PaymentStatus, on_delete=models.RESTRICT, null=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, null=False, blank=False)
    date_pay_due = models.DateTimeField(null=True, blank=True)
    paid_amount = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    date_paid = models.DateTimeField(null=True, blank=True)
    amount_debt = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True)
    doc_name = models.CharField(max_length=20, null=True, blank=True, unique=True, default="Doc name")

    def __str__(self):
        return self.doc_name


class Flow(models.Model):
    """
    finish with attributes of valid_from to
    """
    code = models.CharField(max_length=10, null=False, blank=False)
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f'code({self.code})   name({self.name})   description({self.description})'


class Level(models.Model):
    """
    default = 0 (False)
    """
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    name = models.CharField(max_length=15, null=False, blank=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    started = models.BooleanField(default=0)

    def __str__(self):
        return self.name


class StudentStatus(models.Model):
    """
    Active, Cancelled, Finished,  Moved, Potential, Joined
    """
    name = models.CharField(max_length=15, null=False, blank=False, unique=True)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class Coach(models.Model):
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    user = models.ForeignKey(User, on_delete=models.RESTRICT)

    def __str__(self):
        return self.user.account_name


class Student(models.Model):
    """
    Check the logic of the attributes
    especially creation date
    """
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    coach = models.ForeignKey(Coach, on_delete=models.RESTRICT, blank=True, null=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    student_status = models.ForeignKey(StudentStatus, on_delete=models.RESTRICT)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.account_name
# ===========================================================


class ListCategory(models.Model):
    """
    Task, Bonus
    """
    name = models.CharField(max_length=15, null=False, blank=False, unique=True)
    description = models.TextField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class ListN(models.Model):
    """
    Confirm logic of booleanfield
    """
    task_no = models.CharField(max_length=10, null=False, blank=False)
    name = models.CharField(max_length=70, null=False, blank=False)
    description = models.TextField(blank=True, null=True)
    complex = models.BooleanField(default=0)
    is_active = models.BooleanField(default=1)
    list_category = models.ForeignKey(ListCategory, on_delete=models.RESTRICT)

    def __str__(self):
        return self.name


class TaskCategory(models.Model):
    """
    Main, Additional, Marathon
    """

    name = models.CharField(max_length=10, null=False, blank=False, unique=True)
    description = models.TextField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Task(models.Model):
    """
    check attributes
    """
    code = models.CharField(max_length=10, null=False, blank=False)
    list = models.ForeignKey(ListN, on_delete=models.RESTRICT)
    issued = models.BooleanField(default=0)
    link_in_base = models.TextField(null=True, blank=True)
    link_in_chat = models.TextField(null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    issue_date = models.DateTimeField()
    due_date = models.DateTimeField()

    def __str__(self):
        return self.code


class BonusCategory(models.Model):
    """
    Artifact, Team, Marathone
    """
    name = models.CharField(max_length=15, null=False, blank=False, unique=True)
    description = models.TextField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class TaskCodeLastNo(models.Model):
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=True, blank=True)
    task_sequence_no = models.IntegerField(default=0, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=True, blank=True)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT, null=True, blank=True)
    bonus_sequence_no = models.IntegerField(default=0, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    level_sequence_no = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        if self.task_category:
            return self.task_sequence_no
        elif self.bonus_category:
            return self.bonus_sequence_no
        else:
            return self.level_sequence_no


class AdditionalMaterials(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, blank=True, null=True)
    link = models.TextField(null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    task = models.ForeignKey(Task, on_delete=models.RESTRICT, blank=True, null=True)

    def __str__(self):
        return self.name


class PlanedTaskAmount(models.Model):
    amount = models.IntegerField(default=0)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, blank=True, null=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)

    def __str__(self):
        return self.amount
# =========================================================


class Bonus(models.Model):
    code = models.CharField(max_length=15, null=False, blank=False)
    list = models.ForeignKey(ListN, on_delete=models.RESTRICT)
    amount = models.IntegerField()
    issued = models.BooleanField(default=0)
    issued_date = models.DateTimeField()
    points = models.CharField(max_length=10, null=False, blank=False)
    link_in_base = models.TextField(null=True, blank=True)
    link_in_chat = models.TextField(null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT)

    def __str__(self):
        return self.code


class PlannedBonusesAmount(models.Model):
    amount = models.IntegerField(null=False, blank=False, default=0)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT, null=False, blank=False)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, blank=True, null=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        return self.amount
# ==============================================================================


class Team(models.Model):
    """
    Teams arranged by Students in the course.
    """
    name = models.CharField(max_length=30, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        return self.name


class TeamMember(models.Model):
    """
    List of Students in the Team.  Coach might be a memeber of several teams.
     Coach is a memeber of a team if Student_ID == Coach_ID
    """
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    student = models.ForeignKey(Student, on_delete=models.RESTRICT, null=True, blank=True)
    coach = models.ForeignKey(Coach, on_delete=models.RESTRICT, null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        if self.student:
            return self.student.user.name
        elif self.coach:
            return self.coach.user.name


class FulfillmentStatus(models.Model):
    """
    Task fulfillment statuses like NEW, DONE, IN_PROGRESS.
    """
    name = models.CharField(max_length=15, null=False, blank=False)
    description = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name


class Fulfillment(models.Model):
    """
    Log of task fulfillment by Students.
    """
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    task = models.ForeignKey(Task, on_delete=models.RESTRICT, null=False, blank=False)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    fulfillment_status = models.ForeignKey(FulfillmentStatus, on_delete=models.RESTRICT, null=False, blank=False)
    date = models.DateTimeField(auto_now_add=True)
    student = models.ForeignKey(Student, on_delete=models.RESTRICT, null=False, blank=False)
    link_in_chat = models.TextField(null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)
    advice = models.CharField(max_length=100, null=True, blank=True)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        return self.task.name
# =============================================================================


class EarnedBonus(models.Model):
    """
    Log of bonuses earned by Students.
    """
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    bonus = models.ForeignKey(Bonus, on_delete=models.RESTRICT, null=False, blank=False)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    student = models.ForeignKey(Student, on_delete=models.RESTRICT, null=False, blank=False)
    link_in_chat = models.TextField(null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)

    def __str__(self):
        return self.bonus.name
#====================================================================================


class KPI(models.Model):
    """
    Agreed Key Performance Indicators (KPIs)
    """
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=True, blank=True)
    name = models.CharField(max_length=30, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    boundary = models.CharField(max_length=15, null=True, blank=True)

    def __str__(self):
        return self.name


class KPIScale(models.Model):
    """
    Agreed Key Performance Indicators (KPIs),   Logic to be updated!!!!!
    """
    kpi_id_boundary1 = models.ForeignKey(KPI, on_delete=models.RESTRICT, null=True, blank=True,
                                         related_name="kpi_id_boundary1")
    kpi_id_boundary2 = models.ForeignKey(KPI, on_delete=models.RESTRICT, null=True, blank=True,
                                         related_name="kpi_id_boundary2")
    scale_color = models.CharField(max_length=30, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    boundary_1_sign = models.CharField(max_length=5, null=True, blank=True)
    boundary_2_sign = models.CharField(max_length=5, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.scale_color

#=========================================================================


class StudentTaskTotal(models.Model):
    """
    Total amounts on Tasks fulfillment progress calculated by Student
    """
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.RESTRICT, null=False, blank=False)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=False, blank=False)
    level_issued = models.IntegerField(null=True, blank=True)
    flow_issued = models.IntegerField(null=True, blank=True)
    fulfillment_status = models.ForeignKey(FulfillmentStatus, on_delete=models.RESTRICT, null=False, blank=False)
    level_amount = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_amount = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    level_debt_against_kpi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_debt_against_kpi = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    kpi_scale = models.ForeignKey(KPIScale, on_delete=models.RESTRICT, null=True, blank=True)
    level_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.student.user.account_name


class TeamTaskTotal(models.Model):
    """
    Total amounts on Tasks fulfillment progress calculated by Team
    """
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=False, blank=False)
    level_issued = models.IntegerField(null=True, blank=True)
    flow_issued = models.IntegerField(null=True, blank=True)
    fulfillment_status = models.ForeignKey(FulfillmentStatus, on_delete=models.RESTRICT, null=False, blank=False)
    level_amount = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_amount = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    kpi_scale = models.ForeignKey(KPIScale, on_delete=models.RESTRICT, null=True, blank=True)
    level_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.team.name


class FlowTaskTotal(models.Model):
    """
    Total amounts on Tasks fulfillment progress calculated by Course
    """
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    task = models.ForeignKey(Task, on_delete=models.RESTRICT, null=False, blank=False)
    task_category = models.ForeignKey(TaskCategory, on_delete=models.RESTRICT, null=False, blank=False)
    fulfillment_status = models.ForeignKey(FulfillmentStatus, on_delete=models.RESTRICT, null=False, blank=False)
    student_status = models.ForeignKey(StudentStatus, on_delete=models.RESTRICT, null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    level_amount = models.IntegerField(null=True, blank=True)
    flow_amount = models.IntegerField(null=True, blank=True)
    level_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    flow_amount_percent = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.task.name
#================================================================================


class StudentBonusTotal(models.Model):
    """
    Total amounts on earned Bonuses calculated by Student
    """
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    student = models.ForeignKey(Student, on_delete=models.RESTRICT, null=False, blank=False)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT, null=False, blank=False)
    level_issued = models.IntegerField(null=True, blank=True)
    flow_issued = models.IntegerField(null=True, blank=True)
    level_earned = models.IntegerField(null=True, blank=True)
    flow_earned = models.IntegerField(null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    kpi_scale = models.ForeignKey(KPIScale, on_delete=models.RESTRICT, null=True, blank=True)

    def __str__(self):
        return self.student.name


class TeamBonusTotal(models.Model):
    """
    Total amounts on earned Bonuses calculated by Team
    """
    level = models.ForeignKey(Level, on_delete=models.RESTRICT, null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.RESTRICT, null=True, blank=True)
    bonus_category = models.ForeignKey(BonusCategory, on_delete=models.RESTRICT, null=False, blank=False)
    level_issued = models.IntegerField(null=True, blank=True)
    flow_issued = models.IntegerField(null=True, blank=True)
    level_earned = models.IntegerField(null=True, blank=True)
    flow_earned = models.IntegerField(null=True, blank=True)
    flow = models.ForeignKey(Flow, on_delete=models.RESTRICT, null=False, blank=False)
    kpi_scale = models.ForeignKey(KPIScale, on_delete=models.RESTRICT, null=True, blank=True)

    def __str__(self):
        return self.team.name


