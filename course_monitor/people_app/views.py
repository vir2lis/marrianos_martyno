from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from django.contrib.auth.models import Group
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin


from course_monitor.people_app.decorators import unauthenticated_user, allowed_users

from course_monitor.people_app.forms import RegisterUserForm, CreateCityForm, UpdateCityTimeZoneForm, \
    CreateTimeZoneForm, \
    CreateCityTimeZoneForm, UpdateListCatForm, UpdateFlowForm, UpdateTaskCategoryForm, UpdateBonusCategoryForm, \
    CreateFlowForm, UpdatePaymentStatusForm, CreateLevelForm, UpdateLevelForm, CreateStudentStatusForm, UpdateUserForm, \
    CreateStudentForm, CreatePaymentForm

from course_monitor.people_app.models import CityTimeZone, City, TimeZone, ListCategory, Flow, PaymentStatus, \
    TaskCategory, BonusCategory, Level, User, StudentStatus, Coach, Student, Payment, UserSocialNetwork


@unauthenticated_user
def log_in(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('base')
        else:
            messages.success(request, 'bad username or password, try again...')
    return render(request, 'login.html')


@login_required(login_url='login')
@allowed_users(allowed_roles=['Guest', 'Owner'])
def base(request):
    return render(request, 'content.html')


def log_out(request):
    logout(request)
    return redirect('login')


@login_required(login_url='login')
def register_user(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='Guest')
            user.groups.add(group)
            messages.success(request, 'Your account was created for' + username)
            return redirect('registration')
        else:
            messages.error(request, "Error occurred")
    else:
        form = RegisterUserForm()
    return render(request, 'registration.html', {'form': form})


@login_required(login_url='login')
@allowed_users(allowed_roles=['Guest', 'Owner'])
def permissions_test(request):
    return render(request, 'test_for_permissions.html', {
    })


class ViewCityTimezone(LoginRequiredMixin, ListView):
    login_url = '/'
    model = CityTimeZone
    template_name = 'cities.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cities'] = CityTimeZone.objects.order_by('city__name')
        return context


class CreateCityTimeZone(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = CityTimeZone
    form_class = CreateCityTimeZoneForm
    template_name = 'create_city.html'
    success_url = '/cities/'


class UpdateCityTimeZone(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = CityTimeZone
    form_class = UpdateCityTimeZoneForm
    template_name = 'update_city.html'
    success_url = '/cities/'


class DeleteCityTimeZone(LoginRequiredMixin, DeleteView):
    login_url = '/'
    model = CityTimeZone
    template_name = 'delete_msg.html'
    success_url = '/cities/'


class CreateCity(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = City
    form_class = CreateCityForm
    template_name = 'common/create.html'
    success_url = '/cities/'


class CreateTimeZone(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = TimeZone
    form_class = CreateTimeZoneForm
    template_name = 'common/create.html'
    success_url = '/cities/'


class ViewListCat(LoginRequiredMixin, ListView):
    login_url = '/'
    model = ListCategory
    template_name = 'common/items_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = ListCategory.objects.order_by('name')
        return context


class ViewBonusCat(LoginRequiredMixin, ListView):
    login_url = '/'
    model = BonusCategory
    template_name = 'common/items_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = BonusCategory.objects.order_by('name')
        return context


class ViewTaskCat(LoginRequiredMixin, ListView):
    login_url = '/'
    model = TaskCategory
    template_name = 'common/items_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = TaskCategory.objects.order_by('name')
        return context


class ViewFlow(LoginRequiredMixin, ListView):
    login_url = '/'
    model = Flow
    template_name = 'flow.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['flows'] = Flow.objects.order_by('name')
        return context


class UpdateListCat(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = ListCategory
    form_class = UpdateListCatForm
    template_name = 'common/update.html'
    success_url = '/list_category/'


class FlowCreate(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = Flow
    form_class = CreateFlowForm
    template_name = 'common/create.html'
    success_url = '/flow/'


class FlowUpdate(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = Flow
    form_class = UpdateFlowForm
    template_name = 'common/update.html'
    success_url = '/flow/'


class FlowDelete(LoginRequiredMixin, DeleteView):
    login_url = '/'
    model = Flow
    template_name = 'delete_msg.html'
    success_url = '/flow/'


class UpdateTaskCategory(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = TaskCategory
    form_class = UpdateTaskCategoryForm
    template_name = 'common/update.html'
    success_url = '/task_category/'


class UpdateBonusCategory(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = BonusCategory
    form_class = UpdateBonusCategoryForm
    template_name = 'common/update.html'
    success_url = '/bonus_category/'


class ViewPaymentStatus(LoginRequiredMixin, ListView):
    login_url = '/'
    model = PaymentStatus
    template_name = 'common/items_category.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = PaymentStatus.objects.order_by('name')
        return context


class UpdatePaymentStatus(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = PaymentStatus
    form_class = UpdatePaymentStatusForm
    template_name = 'common/update.html'
    success_url = '/payment_status/'


class ViewLevel(LoginRequiredMixin, ListView):
    login_url = '/'
    model = Level
    template_name = 'level.html'


class CreateLevel(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = Level
    form_class = CreateLevelForm
    template_name = 'common/create.html'
    success_url = '/'


class UpdateLevel(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = Level
    form_class = UpdateLevelForm
    template_name = 'common/update.html'
    success_url = '/'


class DeleteLevel(LoginRequiredMixin, DeleteView):
    login_url = '/'
    model = Level
    template_name = 'delete_msg.html'
    success_url = '/'


class ViewUser(LoginRequiredMixin, ListView):
    login_url = '/'
    model = User
    template_name = 'users.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['Students'] = Student.objects.all()
        context['Coaches'] = Coach.objects.all()
        return context


class DeleteUser(LoginRequiredMixin, DeleteView):
    login_url = '/'
    model = User
    template_name = 'delete_msg.html'
    success_url = '/'


class ViewStudentStatus(LoginRequiredMixin, ListView):
    login_url = '/'
    model = StudentStatus
    template_name = 'student_status.html'
    context_object_name = 'categories'


class CreateStudentStatus(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = StudentStatus
    form_class = CreateStudentStatusForm
    template_name = 'common/create.html'
    success_url = '/'


class UpdateStudentStatus(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = StudentStatus
    form_class = UpdatePaymentStatusForm
    template_name = 'common/update.html'
    success_url = '/'


class DeleteStudentStatus(LoginRequiredMixin, DeleteView):
    login_url = '/'
    model = StudentStatus
    template_name = 'delete_msg.html'
    success_url = '/'


class UpdateUser(LoginRequiredMixin, UpdateView):
    login_url = '/'
    model = User
    form_class = UpdateUserForm
    template_name = 'user_update.html'
    success_url = '/'


class ViewPayment(LoginRequiredMixin, ListView):
    login_url = '/'
    model = Payment
    template_name = 'payment.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['payments'] = Payment.objects.order_by('doc_name')
        return context


class PaymentCreate(LoginRequiredMixin, CreateView):
    login_url = '/'
    model = Payment
    form_class = CreatePaymentForm
    template_name = 'common/create.html'
    success_url = '/payment/'


@login_required(login_url='login')
def create_student(request, pk):
    _user = User.objects.get(id=pk)
    if request.method == 'POST':
        form = CreateStudentForm(request.POST)
        if form.is_valid():
            student = form.save(commit=False)
            student.user = _user
            student.save()
            group = Group.objects.get(name='Student')
            last_group = _user.groups.first()
            _user.groups.remove(last_group)
            _user.groups.add(group)
            messages.success(request, f'Your account was created for {_user}')
            return redirect(f'/create_student/{pk}')
        else:
            messages.error(request, "Error occurred")
    else:
        form = CreateStudentForm()
    return render(request, 'create_student.html', {
        'form': form
    })


# class CreateSocialNetworkLink(LoginRequiredMixin, CreateView):
#     login_url = '/'
#     model = UserSocialNetwork
#     form_class = CreateSocialNetworkLinkForm
#     template_name = 'common/create.html'
#     success_url = '/base/'

# def create_coach(request, pk):
#     '''
#     Finish this one, continue on forms and urls and template.
#     '''
#     pass


# def home_page(request):
#     # from .models import UserRole, PaymentStatus, StudentStatus, ListCategory, TaskCategory, BonusCategory, \
#     #     FulfillmentStatus
#     #
#     # user_role = ['coach', 'owner', 'student', 'guest']
#     # payment_status = ["full", 'partial', 'free', 'waiting']
#     # student_status = ['active', 'cancelled', 'finished', 'moved', 'potential', 'joined']
#     # list_category = ['task', 'bonus']
#     # task_category = ['main', 'additional', 'marathon']
#     # bonus_category = ['artifact', 'team', 'marathon']
#     # fulfillment_status = ['new', 'done', 'in_progress']
#     # for i in user_role:
#     #     u = UserRole(name=i)
#     #     u.save()
#     # for i in payment_status:
#     #     u = PaymentStatus(name=i)
#     #     u.save()
#     # for i in student_status:
#     #     u = StudentStatus(name=i)
#     #     u.save()
#     # for i in list_category:
#     #     u = ListCategory(name=i)
#     #     u.save()
#     # for i in task_category:
#     #     u = TaskCategory(abbreviation=i)
#     #     u.save()
#     # for i in bonus_category:
#     #     u = BonusCategory(abbreviation=i)
#     #     u.save()
#     # for i in fulfillment_status:
#     #     u = FulfillmentStatus(name=i)
#     #     u.save()
#     return HttpResponse("<a href="home/">Home</a>
# <a href="base/">Base</a>")
# # Create your views here.
