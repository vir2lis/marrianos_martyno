from django.urls import path
# from .views import *
from .views import log_in, base, log_out, register_user, permissions_test, ViewCityTimezone, UpdateCityTimeZone, \
    DeleteCityTimeZone, CreateCity, CreateTimeZone, CreateCityTimeZone, ViewListCat, \
    UpdateListCat, FlowCreate, FlowUpdate, FlowDelete, ViewTaskCat, UpdateTaskCategory, UpdateBonusCategory, \
    ViewPaymentStatus, \
    UpdatePaymentStatus, ViewBonusCat, ViewTaskCat, ViewFlow, ViewLevel, CreateLevel, UpdateLevel, DeleteLevel, \
    ViewUser, DeleteUser, ViewStudentStatus, CreateStudentStatus, UpdateStudentStatus, DeleteStudentStatus, UpdateUser, \
    create_student, ViewPayment, PaymentCreate

urlpatterns = [
    path('', log_in, name='login'),
    path('base/', base, name='base'),
    path('logout/', log_out, name='logout'),
    path('register/', register_user, name='registration'),
    path('update_user/<pk>', UpdateUser.as_view(extra_context={
    'title': 'User Update', 'cancel': 'base'}
    ), name='update_user'),
    path('permissions/', permissions_test, name='permissions'),
    path('cities/', ViewCityTimezone.as_view(), name='cities'),
    path('cities/<pk>', UpdateCityTimeZone.as_view(extra_context={'title':'City','cancel': 'cities'}),
         name='cities_update'),
    path('cities_del/<pk>', DeleteCityTimeZone.as_view(), name='cities_del'),
    path('reg_city/', CreateCity.as_view(), name='reg_city'),
    path('reg_timezone/', CreateTimeZone.as_view(), name='reg_timezone'),
    path('reg_citytimezone/', CreateCityTimeZone.as_view(), name='reg_citytimezone'),
    path('list_category/', ViewListCat.as_view(extra_context={'title': 'List Category', 'update': 'list_update'}),
         name='list_cat'),
    path('list_update/<pk>',
         UpdateListCat.as_view(extra_context={'title': 'List Category Update', 'cancel': 'list_cat'}),
         name='list_update'),
    path('flow_create/', FlowCreate.as_view(), name='flow_create'),
    path('flow_update/<pk>', FlowUpdate.as_view(extra_context={'title':'Flow'
                                                               ,'cancel':'flow'}), name='flow_update'),
    path('flow_delete/<pk>', FlowDelete.as_view(), name='flow_delete'),
    path('payment_status/',
         ViewPaymentStatus.as_view(extra_context={'title': 'Payment status', 'update': 'payment_status_update'}),
         name='payment_status'),
    path('payment_status_update/<pk>',
         UpdatePaymentStatus.as_view(extra_context={'title': 'Payment Status Update', 'cancel': 'payment_status'}),
         name='payment_status_update'),
    path('bonus_category/', ViewBonusCat.as_view(extra_context={'title': 'Bonus Category',
                                                                'update': 'bonus_category_update'}), name='bonus_cat'),
    path('bonus_category_update/<pk>',
         UpdateBonusCategory.as_view(extra_context={'title': 'Bonus Category Update', 'cancel': 'bonus_cat'}),
         name='bonus_category_update'),
    path('task_category/', ViewTaskCat.as_view(extra_context={'title': 'Task Category',
                                                              'update': 'task_category_update'}), name='task_category'),
    path('task_category_update/<pk>',
         UpdateTaskCategory.as_view(extra_context={'title': 'Task Category Update', 'cancel': 'task_category'}),
         name='task_category_update'),
    path('flow/', ViewFlow.as_view(), name='flow'),
    path('level/', ViewLevel.as_view(), name='level'),
    path('create_level/', CreateLevel.as_view(), name='create_level'),
    path('update_level/<pk>', UpdateLevel.as_view(), name='update_level'),
    path('delete_level/<pk>', DeleteLevel.as_view(), name='delete_level'),
    path('users/', ViewUser.as_view(), name='users'),
    path('delete_user/<pk>', DeleteUser.as_view(), name='delete_user'),
    path('student_status/', ViewStudentStatus.as_view(extra_context={'title': 'Student Status',
                                                                     'update': 'update_student_status'}),
         name='student_status'),
    path('create_student_status/', CreateStudentStatus.as_view(), name='create_student_status'),

    path('update_student_status/<pk>', UpdateStudentStatus.as_view(extra_context={'title': 'Student Status Update', 'cancel': 'student_status'}), name='update_student_status'),
    path('delete_student_status/<pk>', DeleteStudentStatus.as_view(), name='delete_student_status'),
    path('create_student/<pk>', create_student, name='create_student'),
    path('payment/', ViewPayment.as_view(), name='payment'),
    path('payment_create/', PaymentCreate.as_view(), name='payment_create'),

]
