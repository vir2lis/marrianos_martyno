import pytest
from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.messages import get_messages
from django.urls import reverse

from course_monitor.people_app.forms import RegisterUserForm
from course_monitor.people_app.models import User, City, CityTimeZone, TimeZone, ListCategory, Flow, TaskCategory, \
    BonusCategory, PaymentStatus


def test_login_get(client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'login.html'


@pytest.mark.django_db
def test_login_post(client):
    Group.objects.create(name='Guest')
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    resp = client.post('/', {'username': 'test@example.com', 'password': '123!2@kkl'})
    assert resp.status_code == 302
    assert resp.headers['location'] == '/base/'


@pytest.mark.django_db
def test_login_post_bad(client):
    Group.objects.create(name='Guest')
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    resp = client.post('/', {'username': 'test@example.com', 'password': '123!2@kkl1'})
    assert resp.status_code == 200
    assert b'bad username or password, try again...' in resp.content


@pytest.mark.django_db
def test_register_user_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.get('/register/')
    assert resp.status_code == 200
    assert isinstance(resp.context['form'], RegisterUserForm)


@pytest.mark.django_db
def test_register_user_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    Group.objects.create(name='Guest')
    resp = client.post('/register/', {'username': 'test1', 'email': 'test1@example.com', 'account_name': '@test',
                                      'password1': '123!2@kkl', 'password2': '123!2@kkl'})
    assert resp.status_code == 302, resp.context['form'].errors.as_json()
    assert User.objects.filter(username='test').exists()


def test_base(client):
    resp = client.get('/base/')
    assert resp.status_code == 302
    assert resp.content == b''


def test_logout(client):
    resp = client.get('/logout/')
    assert resp.status_code == 302
    assert resp.headers['location'] == '/'


@pytest.mark.django_db
def test_view_citytimezone(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.get('/cities/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'cities.html'
    assert resp.templates[1].name == 'base.html'


@pytest.mark.django_db
def test_create_city_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.get('/reg_city/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/create.html'
    assert resp.templates[1].name == 'base.html'


@pytest.mark.django_db
def test_create_city_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.post('/reg_city/', {'name': 'Klaipeda', 'description': 'Lithuania'})
    assert resp.status_code == 302
    assert resp.headers['location'] == '/cities/'
    assert City.objects.filter(name='Klaipeda').exists()


@pytest.mark.django_db
def test_update_citytimezone_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    city = City.objects.create(name='Klaipeda')
    time_zone = TimeZone.objects.create(name='UTC')
    city_time_zone = CityTimeZone.objects.create(city=city, time_zone=time_zone, offset='+2')
    resp = client.get(f'/cities/{city_time_zone.id}')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'update_city.html'
    assert CityTimeZone.objects.filter(city_id__name='Klaipeda').exists()


@pytest.mark.django_db
def test_update_city_time_zone_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    assert City.objects.count() == 0
    city = City.objects.create(name='Vilnius', description='Lithuania')
    time_zone = TimeZone.objects.create(name='GMT', description='Game time')
    offset = '+2'
    city_time_zone = CityTimeZone.objects.create(city=city, time_zone=time_zone, offset=offset)
    resp1 = client.post(f'/cities/{city_time_zone.id}',
                        {'city': city.id, 'time_zone': time_zone.id, 'offset': '+3'})
    assert resp1.status_code == 302, resp1.context['form'].errors.as_json()
    assert resp1.headers['location'] == '/cities/'
    city_time_zone.refresh_from_db()
    assert city_time_zone.offset == '+3'


@pytest.mark.django_db
def test_delete_citytimezone_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    city = City.objects.create(name='Klaipeda')
    time_zone = TimeZone.objects.create(name='UTC')
    city_time_zone = CityTimeZone.objects.create(city=city, time_zone=time_zone, offset='+2')
    resp = client.get(f'/cities_del/{city_time_zone.id}')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'delete_msg.html'


@pytest.mark.django_db
def test_delete_citytimezone_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    city = City.objects.create(name='Klaipeda')
    time_zone = TimeZone.objects.create(name='UTC')
    city_time_zone = CityTimeZone.objects.create(city=city, time_zone=time_zone, offset='+2')
    resp = client.post(f'/cities_del/{city_time_zone.id}')
    assert resp.status_code == 302
    assert resp.headers['location'] == '/cities/'
    assert TimeZone.objects.filter(id=city_time_zone.id).exists()


@pytest.mark.django_db
def test_create_timezone_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.get('/reg_timezone/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/create.html'
    assert resp.templates[1].name == 'base.html'


@pytest.mark.django_db
def test_create_timezone_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    resp = client.post('/reg_timezone/', {'name': 'GMT', 'description': 'Grand money tree'})
    assert resp.status_code == 302
    assert resp.headers['location'] == '/cities/'
    assert TimeZone.objects.filter(name='GMT').exists()


@pytest.mark.django_db
def test_view_list_category(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    cat1 = ListCategory.objects.create(name='Random', description='1')
    cat2 = ListCategory.objects.create(name='Random2', description='12')
    resp = client.get('/list_category/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/items_category.html'
    assert list(resp.context['categories']) == [cat1, cat2]


@pytest.mark.django_db
def test_update_list_category_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    list_category = ListCategory.objects.create(name='Task', description='Some task')
    resp = client.get(f'/list_update/{list_category.id}')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/update.html'
    assert ListCategory.objects.filter(name='Task').exists()


@pytest.mark.django_db
def test_update_list_category_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    list_category = ListCategory.objects.create(name='Task', description='Some task')
    resp = client.post(f'/list_update/{list_category.id}', {
        'description': 'Bonus description'
    })
    assert resp.status_code == 302
    assert resp.headers['location'] == '/list_category/'
    assert ListCategory.objects.filter(description='Bonus description')


@pytest.mark.django_db
def test_update_flow_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    flow = Flow.objects.create(code='NLU', name='North Liberal Utility')
    resp = client.get(f'/flow_update/{flow.id}')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/update.html'
    assert flow.name == 'North Liberal Utility'


@pytest.mark.django_db
def test_update_flow_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    flow = Flow.objects.create(code='NLU', name='North Liberal Utility')
    resp = client.post(f'/flow_update/{flow.id}', {
        'code': 'GMT',
        'name': 'Game Member Transaction'
    })
    assert resp.status_code == 302
    flow.refresh_from_db()
    assert flow.code == 'GMT' and flow.name == 'Game Member Transaction'
    assert resp.headers['location'] == '/flow/'


@pytest.mark.django_db
def test_update_task_category_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    task_category = TaskCategory.objects.create(name='Additional',
                                                description='Some additional description')
    resp = client.get(f'/task_category_update/{task_category.id}')
    assert resp.status_code == 200
    assert task_category.name == 'Additional'
    assert resp.templates[0].name == 'common/update.html'


@pytest.mark.django_db
def test_update_task_category_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    task_category = TaskCategory.objects.create(name='Additional',
                                                description='Some additional description')
    resp = client.post(f'/task_category_update/{task_category.id}', {
        'description': 'Runners gonna run'
    })
    assert resp.status_code == 302
    assert task_category.name == 'Additional'
    task_category.refresh_from_db()
    assert resp.headers['location'] == '/task_category/'


@pytest.mark.django_db
def test_update_bonus_category_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    bonus_category = BonusCategory.objects.create(name='Artifact',
                                                  description='Some artifact')
    resp = client.get(f'/bonus_category_update/{bonus_category.id}')
    assert resp.status_code == 200
    assert bonus_category.name == 'Artifact'
    assert resp.templates[0].name == 'common/update.html'


@pytest.mark.django_db
def test_update_bonus_category_post(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    bonus_category = BonusCategory.objects.create(name='Artifact',
                                                  description='Some additional description')
    resp = client.post(f'/bonus_category_update/{bonus_category.id}', {
        'description': 'Runners gonna run'
    })
    assert resp.status_code == 302
    assert bonus_category.name == 'Artifact'
    bonus_category.refresh_from_db()
    assert bonus_category.description == 'Runners gonna run'
    assert resp.headers['location'] == '/bonus_category/'


@pytest.mark.django_db
def test_view_payment_status_get(client):
    User.objects.create_user(username='test', email='test@example.com', password='123!2@kkl')
    client.login(email='test@example.com', password='123!2@kkl')
    PaymentStatus.objects.create(name='test_payment')
    resp = client.get('/payment_status/')
    assert resp.status_code == 200
    assert resp.templates[0].name == 'common/items_category.html'
    assert PaymentStatus.objects.get(name='test_payment')
    assert 'categories' in resp.context
    assert list(resp.context['categories']) == [PaymentStatus.objects.get(name='test_payment')]
